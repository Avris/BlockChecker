-- Up

CREATE TABLE Users (
    id TEXT NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    checkedAt INTEGER,
    blocks INTEGER
);

-- Down

DROP TABLE Users;
