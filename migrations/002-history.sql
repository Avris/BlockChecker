-- Up

CREATE TABLE UserHistory (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    userId TEXT NOT NULL,
    checkedAt INTEGER,
    blocks INTEGER,
    position INTEGER
);

-- Down

DROP TABLE UserHistory;
