module.exports = function () {
    return require('./layout')(`
        <div class="flex-grow-1 d-flex flex-column justify-content-center align-items-center">
            <header class="py-4">
                <h1>
                    Privacy
                    <small class="text-muted">Block Checker</small>
                </h1>
                <p>It's a simple tool. It keeps only the minimum set of data that it needs to do its job:</p>
                <ul>
                    <li>your Twitter ID and handle,</li>
                    <li>number of accounts blocked,</li>
                    <li>date and time when the check was performed.</li>                
                </ul>
                <p>
                    After each time you check your number of blocked acounts
                    Block Checker stores the result for 15 minutes in a cookie called <code>result</code>
                    so that if you reload the tab it doesn't have to ask Twitter every time. 
                </p>
                <p>
                    It also uses a cookie called <code>connect.sid</code>
                    because it's necessary for Twitter authentication.
                </p>
                <p>
                    That's it. It doesn't track you. It doesn't share your data with any third parties.
                </p>
                <p>
                    If you have any questions or requests regarding your personal data,
                    feel free to contact me at
                    <a href="mailto:andre@avris.it" target="_blank" rel="noopener">✉ andre@avris.it</a>
                </p>
                
                <p class="mt-5">
                    <a href="/">⬅ Back to the Checker</a>                
                </p>
            </header>
        </div>
    `);
};
