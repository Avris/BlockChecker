module.exports = function (content) {
    return `
        <html>
            <head>
                <meta charset="UTF-8" />
                <title>Block Checker</title>
                <meta name="Content-Type" content="text/html; charset=UTF-8" />
                <meta name="description" content="Check how many people you blocked on Twitter and compare yourself to others." />
                <meta name="keywords" content="blocked, block, ban, twitter, counter" />
                <meta name="author" content="Andre Prusinowski <andre@avris.it>" />

                <meta property="og:type" content="article" />
                <meta property="og:title" content="Block Checker" />
                <meta property="og:url" content="${process.env.URL}" />
                <meta property="og:image" content="${process.env.URL}/banner.png" />

                <meta name="twitter:card" content="summary_large_image"/>
                <meta name="twitter:title" content="Block Checker" />
                <meta name="twitter:description" content="Check how many people you blocked on Twitter and compare yourself to others." />
                <meta name="twitter:image" content="https://blockchecker.avris.it/banner.png" />

                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
                <link href="https://fonts.googleapis.com/css?family=Philosopher:400,700&display=swap" rel="stylesheet">
                <link rel="shortcut icon" href="${process.env.URL}/favicon.png"/>
                
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                
                <style>
                    body {
                        font-family: Philosopher, Ubuntu, sans-serif;
                    }
                    
                    main {
                        min-height: 100vh;
                    }
                    
                    header {
                        max-width: 40rem;
                    }
                    
                    table.table {
                        width: auto;
                    }
                    
                    .emoji {
                        height: 1em;
                    }
                    
                    .table-history {
                        display: block;
                        max-height: 20rem;
                        overflow-y: auto;
                    }
                    
                    :root {
                        --scrollbar-track-color: transparent;
                        --scrollbar-color: rgba(0,0,0,.2);
                    
                        --scrollbar-size: .375rem;
                        --scrollbar-minlength: 1.5rem;
                    }
                    .overflowing-element::-webkit-scrollbar {
                        height: var(--scrollbar-size);
                        width: var(--scrollbar-size);
                    }
                    .overflowing-element::-webkit-scrollbar-track {
                        background-color: var(--scrollbar-track-color);
                    }
                    .overflowing-element::-webkit-scrollbar-thumb {
                        background-color: var(--scrollbar-color);
                    }
                    .overflowing-element::-webkit-scrollbar-thumb:vertical {
                        min-height: var(--scrollbar-minlength);
                    }
                    .overflowing-element::-webkit-scrollbar-thumb:horizontal {
                        min-width: var(--scrollbar-minlength);
                    }
                    
                    
                    #plot {
                        text-align: center;
                    }
                    @media (max-width: 1200px) {
                        #plot {
                            width: 100%;
                            height: 30rem;
                        }
                    }
                    @media (min-width: 1200px) {
                        #plot {
                            width: 60rem;
                            height: 40rem;
                        }
                    }
                </style>
            </head>
            
            <body class="mx-4">
                <main class="d-flex flex-column">
                    ${content}
    
                    <footer class="border-top py-4 mt-4 text-center d-md-flex justify-content-between">
                        <div>
                            Made with ❤ by
                            <a href="https://twitter.com/AvrisIT" target="_blank" rel="noopener" class="text-nowrap">Avris 🇪🇺 🏳️‍🌈</a>
                            <a href="https://paypal.me/AndreAvris" target="_blank" rel="noopener" class="text-nowrap ml-4">🍺 Buy me a beer</a>
                        </div>
                        <div class="mt-3 mt-md-0">
                            <a href="/privacy" class="text-nowrap mr-4">🔒 Privacy</a>
                            <a href="https://gitlab.com/Avris/BlockChecker" target="_blank" rel="noopener" class="text-nowrap">👨🏽‍💻 Source code</a>
                        </div>
                    </footer>
                </main>
            </body>
            
            <script src="https://twemoji.maxcdn.com/v/latest/twemoji.min.js" crossorigin="anonymous"></script>
            <script>
                twemoji.parse(document.querySelector('main'));
            </script>
        </html>
    `;
};
