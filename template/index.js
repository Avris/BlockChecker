const dateFormat = require('dateformat');

const ordinal = function (number) {
    if (Math.floor(number / 10) % 10 === 1) {
        return 'th';
    }

    if (number % 10 === 1) {
        return 'st';
    }
    if (number % 10 === 2) {
        return 'nd';
    }
    if (number % 10 === 3) {
        return 'rd';
    }

    return 'th';
};

const dt = function (timestamp) {
    return dateFormat(new Date(timestamp * 1000), 'yyyy-mm-dd HH:MM');
};

const rankingRow = function (rowPosition, userPosition, blocks) {
    return `
        <tr>
            <th>
                ${rowPosition}<sup>${ordinal(rowPosition)}</sup>
            </th>
            <td>
                ${blocks} blocks ${rowPosition === userPosition ? '<span class="badge badge-primary">← you</span>' : ''}
            </td>
        </tr>
    `;
};

const ranking = function (result) {
    const rows = [];

    for (let i in result.ranking) {
        rows.push(rankingRow(parseInt(i) + 1, result.position, result.ranking[i]));
    }

    if (result.position > 11) {
        rows.push(`<tr><th>…</th><td></td></tr>`);
    }

    if (result.position > 10) {
        rows.push(rankingRow(result.position, result.position, result.blocks));
    }

    rows.push(`<tr><th>…</th><td></td></tr>`);

    return rows.join('');
};

const history = function (history) {
    const rows = [];

    for (let i in history) {
        rows.push(`
            <tr>
                <th>
                    ${dt(history[i].checkedAt)}
                </th>
                <td>
                    ${history[i].blocks} blocks
                </td>
                <td>
                    ${history[i].position}<sup>${ordinal(history[i].position)}</sup> position
                </td>
            </tr>
        `);
    }

    return rows.join('');
};

const main = function (result) {
    if (!result) {
        return `
            <div>
                <a href="/login" class="btn btn-outline-primary btn-lg btn-block">
                    🤔 Check
                </a>
            </div>
        `;
    }

    return `
        <div class="d-flex flex-column flex-xl-row w-100 justify-content-center">
            <div class="d-flex flex-column align-items-center">
                <div class="alert alert-success">
                    <p>
                        Your account,
                        <a href="https://twitter.com/${result.handle}" target="_blank" rel="noopener">${result.handle}</a>,
                        has blocked
                        <strong>${result.blocks}</strong>
                        accounts.
                    </p>
                    <p class="mb-0">
                        This means you're in the ${result.position}<sup>${ordinal(result.position)}</sup> position out of ${result.count} people who checked it here.
                    </p>
                    <p class="mt-3">
                        <a href="https://twitter.com/share?text=${encodeURIComponent('I\'ve blocked ' + result.blocks + ' accounts... How about you?')}&amp;url=${encodeURIComponent(process.env.URL)}"
                            target="_blank" rel="noopener"
                            class="btn btn-outline-success btn-block"
                             >
                             🕊 Share
                         </a>
                    </p>
                </div>
                <div class="d-md-flex">
                    <table class="table table-sm table-borderless mt-4 mx-4">
                        <tr>
                            <th colspan="2" class="h4">Ranking:</th>
                        </tr>
                        ${ranking(result)}            
                    </table>
                    <table class="table table-sm table-borderless mt-4 mx-4 table-history overflowing-element">
                        <tr>
                            <th colspan="2" class="h4">Your history:</th>
                        </tr>
                        ${history(result.history)}
                    </table>
                </div>
            </div>
            ${result.history.length >= 3 ? `
            <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
            <div id="plot"></div>
            <script>
                var resultHistory = ${JSON.stringify(result.history)};
                Plotly.plot(
                    document.getElementById('plot'),
                    [
                        {
                            x: resultHistory.map(r => new Date(r.checkedAt * 1000)),
                            y: resultHistory.map(r => r.blocks),
                            type: 'scatter',
                            mode: 'markers+lines',
                            name: 'Blocks',
                        },
                        {
                            x: resultHistory.map(r => new Date(r.checkedAt * 1000)),
                            y: resultHistory.map(r => r.position),
                            type: 'scatter',
                            mode: 'markers+lines',
                            name: 'Position',
                            yaxis: 'y2',
                        }
                    ],
                    {
                        xaxis: {
                            type: 'date',
                            autorange: true
                        },
                        yaxis: {
                            type: 'linear',
                            autorange: true
                        },
                        yaxis2: {
                            type: 'linear',
                            side: 'right',
                            autorange: true,
                            overlaying: 'y'
                        },
                        showlegend: false,
                    }
                );
            </script>`: ''}
        </div>
    `;
};

module.exports = function (result) {
    return require('./layout')(`
        <div class="flex-grow-1 d-flex flex-column justify-content-center align-items-center">
            <header class="py-4">
                <h1>Block Checker</h1>
                <p>
                    Check how many people you blocked on Twitter and compare yourself to others.
                </p>
            </header>
    
            ${main(result)}
        </div>
    `);
};
