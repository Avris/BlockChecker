const express = require('express');
const app = express();
const http = require('http').Server(app);
const fs = require('fs');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const Twitter = require('./twitter');
const db = require('./db');

dotenv.config({path: __dirname + '/../.env'});

const tw = new Twitter({
    consumerKey: process.env.TWITTER_API_KEY,
    consumerSecret: process.env.TWITTER_SECERT_KEY,
    callbackUrl: process.env.URL + '/login',
});

app.use(bodyParser.raw());
app.use(session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: true,
}));
app.use(cookieParser());
app.use(express.static('public'));

const handleError = err => {
    console.error(err);
    res.send('Something went wrong... Please try again.');
};

app.get('/', async (req, res) => {
    const result = req.cookies.result ? JSON.parse(req.cookies.result) : null;

    if (result) {
        result.history = await db.history(result.userId);
    }

    res.send(require('../template/index')(result, process.env.URL));
});

app.get('/login', async (req, res) => {
    if (!req.query.oauth_token) {
        tw.login().then(({redirectUrl, tokenSecret}) => {
            req.session.tokenSecret = tokenSecret;
            res.redirect(redirectUrl);
        }).catch(handleError);
        return;
    }

    tw.loginCallback(req.query, req.session.tokenSecret).then(user => {
        delete req.session.tokenSecret;

        req.session.user = user;

        tw.checkBlocks(user.userToken, user.userTokenSecret).then(async (ids) => {
            const blocks = ids.length;

            await db.upsert(user.userId, user.userName, Math.floor(Date.now() / 1000), blocks);

            res.cookie('result', JSON.stringify({
                userId: user.userId,
                handle: user.userName,
                blocks: blocks,
                position: await db.position(blocks),
                count: await db.count(),
                ranking: await db.ranking(),
            }), { maxAge: 900000 });

            res.redirect(process.env.URL);
        }).catch(handleError);
    }).catch(handleError);
});

app.get('/privacy', (req, res) => {
    res.send(require('../template/privacy')());
});

const port = parseInt(process.env.PORT || 3000);
http.listen(port, _ => {
    console.log('listening on *:' + port);
});
