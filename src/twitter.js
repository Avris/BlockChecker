// based on https://github.com/feross/login-with-twitter

const crypto = require('crypto');
const fetch = require('node-fetch');
const OAuth = require('oauth-1.0a');
const querystring = require('querystring');

const TW_REQ_TOKEN_URL = 'https://api.twitter.com/oauth/request_token';
const TW_AUTH_URL = 'https://api.twitter.com/oauth/authenticate';
const TW_ACCESS_TOKEN_URL = 'https://api.twitter.com/oauth/access_token';

const checkStatus = res => {
    if (!res.ok) {
        throw res.statusText;
    }

    return res;
};

class Twitter {
    constructor(opts) {
        if (!opts.consumerKey || typeof opts.consumerKey !== 'string') {
            throw 'Invalid or missing `consumerKey` option';
        }
        if (!opts.consumerSecret || typeof opts.consumerSecret !== 'string') {
            throw 'Invalid or missing `consumerSecret` option';
        }
        if (!opts.callbackUrl || typeof opts.callbackUrl !== 'string') {
            throw 'Invalid or missing `callbackUrl` option';
        }

        this.callbackUrl = opts.callbackUrl;

        this._oauth = OAuth({
            consumer: {
                key: opts.consumerKey,
                secret: opts.consumerSecret
            },
            signature_method: 'HMAC-SHA1',
            hash_function: (baseString, key) => {
                return crypto.createHmac('sha1', key).update(baseString).digest('base64')
            },
        });
    }

    login() {
        const requestData = {
            url: TW_REQ_TOKEN_URL,
            method: 'POST',
            data: {
                oauth_callback: this.callbackUrl
            },
        };

        return fetch(requestData.url, {
            method: requestData.method,
            data: requestData.data,
            headers: this._oauth.toHeader(this._oauth.authorize(requestData)),
        }).then(checkStatus)
            .then(res => res.text())
            .then(data => {
                const {
                    oauth_token: token,
                    oauth_token_secret: tokenSecret,
                    oauth_callback_confirmed: callbackConfirmed,
                } = querystring.parse(data);

                if (callbackConfirmed !== 'true') {
                    throw 'Missing `oauth_callback_confirmed` parameter in response';
                }

                return {
                    redirectUrl: `${TW_AUTH_URL}?${querystring.stringify({ oauth_token: token })}`,
                    tokenSecret
                };
            });
    }

    loginCallback(params, tokenSecret) {
        const {
            oauth_token: token,
            oauth_verifier: verifier,
        } = params;

        if (typeof params.denied === 'string' && params.denied.length > 0) {
            throw 'User denied login permission';
        }

        if (typeof params.oauth_token !== 'string' || params.oauth_token.length === 0) {
            throw 'Invalid or missing `oauth_token` parameter for login callback';
        }

        if (typeof params.oauth_verifier !== 'string' || params.oauth_verifier.length === 0) {
            throw 'Invalid or missing `oauth_verifier` parameter for login callback';
        }

        if (typeof tokenSecret !== 'string' || tokenSecret.length === 0) {
            throw 'Invalid or missing `tokenSecret` argument for login callback';
        }

        const requestData = {
            url: TW_ACCESS_TOKEN_URL,
            method: 'POST',
            data: {
                oauth_token: token,
                oauth_token_secret: tokenSecret,
                oauth_verifier: verifier,
            },
        };

        return fetch(requestData.url, {
            method: requestData.method,
            data: requestData.data,
            headers: this._oauth.toHeader(this._oauth.authorize(requestData)),
        }).then(checkStatus)
            .then(res => res.text())
            .then(data => {
                const {
                    oauth_token: userToken,
                    oauth_token_secret: userTokenSecret,
                    screen_name: userName,
                    user_id: userId,
                } = querystring.parse(data.toString());

                return { userName, userId, userToken, userTokenSecret }
            });
    }

    async checkBlocks(key, secret) {
        let ids = [];
        let cursor = -1;
        do {
            const res = await this._checkBlocksPage(key, secret, cursor);
            ids = ids.concat(res.ids);
            cursor = res.next_cursor;
        } while (cursor !== 0);

        return ids;
    }

    _checkBlocksPage(key, secret, cursor) {
        const requestData = {
            url: `https://api.twitter.com/1.1/blocks/ids.json?stringify_ids=true&cursor=${cursor}`,
            method: 'GET',
        };

        return fetch(requestData.url, {
            method: requestData.method,
            headers: this._oauth.toHeader(this._oauth.authorize(requestData, { key, secret })),
        }).then(checkStatus)
            .then(res => res.text())
            .then(text => JSON.parse(text));
    }
}

module.exports = Twitter;
