const sqlite = require('sqlite');
const SQL = require('sql-template-strings');

class DB {
    async get() {
        if (this.db) {
            return this.db;
        }

        this.db = await sqlite.open(__dirname + '/../db.sqlite', { cached: true });
        await this.db.migrate({migrationsPath: __dirname + '/../migrations'});

        return this.db;
    }

    async upsert(id, name, checkedAt, blocks) {
        const db = await this.get();
        await db.get(SQL`INSERT OR REPLACE INTO Users (id, name, checkedAt, blocks) VALUES (${id}, ${name}, ${checkedAt}, ${blocks})`);
        const position = await this.position(blocks);
        await db.get(SQL`INSERT INTO UserHistory (userId, checkedAt, blocks, position) VALUES (${id}, ${checkedAt}, ${blocks}, ${position})`);
    }

    async count() {
        const db = await this.get();
        const result = await db.get(SQL`SELECT COUNT(*) AS v FROM Users`);

        return result.v;
    }

    async position(blocks) {
        const db = await this.get();
        const result = await db.get(SQL`SELECT COUNT(*) + 1 AS v FROM Users WHERE blocks > ${blocks}`);

        return result.v;
    }

    async ranking(blocks) {
        const db = await this.get();
        const result = await db.all(SQL`SELECT blocks FROM Users ORDER BY blocks DESC LIMIT 10`);

        return result.map(row => row.blocks);
    }

    async history(userId) {
        const db = await this.get();

        return await db.all(SQL`SELECT checkedAt, blocks, position FROM UserHistory WHERE userId = ${userId} ORDER BY checkedAt DESC`);
    }
}

module.exports = new DB();
